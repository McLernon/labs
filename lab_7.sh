#!/bin/bash
whoami
id
id root
cat /etc/passwd
cat /etc/passwd | wc -l
cat /etc/log/boot.log.1
cat /var/log/pacman.log
sudo cat /var/log/pacman.log
for i in `groups`; do echo $i; done
cat /etc/passwd | grep -w "root" | cut -d ":" -f6
sudo useradd -m vasya -c "Vasya Testing User"
sudo touch /home/vasya/some.file
sudo -u vasya date >> /home/vasya/some.file
sudo chown vasya:root /home/vasya/some.file
sudo -u vasya sh -c "date >> /home/vasya/some.file"
sudo chsh -s /sbin/nologin vasya
sudo userdel -r vasya
sudo addgroup --group --gid 9999 my_group
cat /etc/group | grep 9999 | cut -d ":" -f4
sudo useradd -m vasya
sudo usermod -a -G sudo vasya
sudo -u vasya /bin/bash -c whoami
chage -W 30 vasya
sudo userdel -r vasya
