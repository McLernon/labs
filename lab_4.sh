#!/bin/bash
cp -R ~/lab_tasks/4/ ~/labs/4
rm -f ~/labs/4/to_remove/*
rm -f ~/labs/4/glob/*txt
cd ~/labs/4/
mv glob/*txt target
touch target/new_file_{1..10}.txt
ls -a glob
cd ~/labs/4/
cp -a target/.hidden* ./
cd glob
for file in *rename_me_{1..10}.log*; do
	mv "$file" "${file/_me/d}"
done

